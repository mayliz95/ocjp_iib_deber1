/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocjp_iib_deber1;

/**
 *
 * @author Mayra
 */
public class OCJP_IIB_Deber1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Object obj = new Object();
        Fruit fr = new Fruit();
        Apple ap = new Apple();
        Citrus ct = new Citrus();
        Orange org = new Orange();
                              
        checkObject(fr, obj, ap, ct, org);
        System.out.println("");
        checkFruit(fr, obj, ap, ct, org);
        System.out.println("");
        checkApple(fr, obj, ap, ct, org);
        System.out.println("");
        checkCitrus(fr, obj, ap, ct, org);
        System.out.println("");
        checkOrange(fr, obj, ap, ct, org);                                                                            
    }
    
    public static void castObjFruit(Fruit f, Object o) throws ExcepcionCast {
        if (o instanceof Fruit) {
            f = (Fruit) o;
            System.out.print("Fruit: OK ");
        } else throw new ExcepcionCast("Fruit: NO ");        
    }

    public static void castObjApple(Apple ap, Object o) throws ExcepcionCast {

        if (o instanceof Apple) {
            ap = (Apple) o;
            System.out.print("Apple: OK ");
        } else throw new ExcepcionCast("Apple: NO ");       
    }

    public static void castObjSqueezable(Squeezable s, Object o) throws ExcepcionCast {

        if (o instanceof Squeezable) {
            s = (Squeezable) o;
            System.out.print("Squeezable: OK ");
        } else throw new ExcepcionCast("Squeezable: NO ");             
    }

    public static void castObjCitrus(Citrus c, Object o) throws ExcepcionCast {
        if (o instanceof Citrus) {
            c = (Citrus) o;
            System.out.print("Citrus: OK ");
        } else throw new ExcepcionCast("Citrus: NO ");       
    }

    public static void castObjOrange(Orange o, Object ob) throws ExcepcionCast {
        if (ob instanceof Orange) {
            o = (Orange) ob;
            System.out.print("Orange: OK ");
        } else throw new ExcepcionCast("Orange: NO ");       
    }

    public static void castFruitFruit(Fruit f, Fruit fr) throws ExcepcionCast {
        if (fr instanceof Fruit) {
            f = fr;
            System.out.print("Fruit: OK ");
        } else throw new ExcepcionCast("Fruit: NO ");        
    }

    public static void castFruitApple(Apple ap, Fruit fr) throws ExcepcionCast {

        if (fr instanceof Apple) {
            ap = (Apple) fr;
            System.out.print("Apple: OK ");
        } else throw new ExcepcionCast("Apple: NO ");       
    }

    public static void castFruitSqueezable(Squeezable s, Fruit fr) throws ExcepcionCast {

        if (fr instanceof Squeezable) {
            s = (Squeezable) fr;
            System.out.print("Squeezable: OK ");
        } else throw new ExcepcionCast("Squeezable: NO ");             
    }

    public static void castFruitCitrus(Citrus c, Fruit fr) throws ExcepcionCast {
        if (fr instanceof Citrus) {
            c = (Citrus) fr;
            System.out.print("Citrus: OK ");
        } else throw new ExcepcionCast("Citrus: NO ");       
    }

    public static void castFruitOrange(Orange o, Fruit fr) throws ExcepcionCast {
        if (fr instanceof Orange) {
            o = (Orange) fr;
            System.out.print("Orange: OK ");
        } else throw new ExcepcionCast("Orange: NO ");       
    }

    public static void castAppleFruit(Apple a, Fruit fr) throws ExcepcionCast {
        if (a instanceof Fruit) {
            fr = (Fruit) a;
            System.out.print("Fruit: OK ");
        } else throw new ExcepcionCast("Fruit: NO ");       
    }
    
    public static void castAppleApple(Apple ap, Apple a) throws ExcepcionCast {

        if (a instanceof Apple) {
            ap = a ;
            System.out.print("Apple: OK ");
        } else throw new ExcepcionCast("Apple: NO ");       
    }

    public static void castAppleSqueezable(Squeezable s, Apple a) throws ExcepcionCast {

        if (a instanceof Squeezable) {
            s = (Squeezable) a;
            System.out.print("Squeezable: OK ");
        } else throw new ExcepcionCast("Squeezable: NO ");             
    }

    public static void castAppleCitrus(Citrus c, Apple a) throws ExcepcionCast {
        if (a.getClass().isInstance(c)) {
            //c = (Citrus) a;
            System.out.print("Citrus: OK ");
        } else throw new ExcepcionCast("Citrus: NO ");       
    }

    public static void castAppleOrange(Orange o, Apple a) throws ExcepcionCast {
        if (a.getClass().isInstance(o)) {
            //o = (Orange) a;
            System.out.print("Orange: OK ");
        } else throw new ExcepcionCast("Orange: NO ");       
    }
    
    public static void castCitrusFruit(Fruit fr, Citrus c) throws ExcepcionCast {
        if (c instanceof Fruit) {
            fr = (Fruit) c;
            System.out.print("Orange: OK ");
        } else throw new ExcepcionCast("Orange: NO ");       
    }
    
    public static void castCitrusApple(Apple ap, Citrus c) throws ExcepcionCast {

        if (!c.getClass().isInstance(ap)) {
            throw new ExcepcionCast("Apple: NO ");       
        }
    }

    public static void castCitrusSqueezable(Squeezable s, Citrus c) throws ExcepcionCast {

        if (c instanceof Squeezable) {
            s = (Squeezable) c;
            System.out.print("Squeezable: OK ");
        } else throw new ExcepcionCast("Squeezable: NO ");             
    }

    public static void castCitrusCitrus(Citrus c, Citrus c1) throws ExcepcionCast {
        if (c1 instanceof Citrus) {
            c = c1;
            System.out.print("Citrus: OK ");
        } else throw new ExcepcionCast("Citrus: NO ");       
    }

    public static void castCitrusOrange(Orange o, Citrus c) throws ExcepcionCast {
        if (c instanceof Orange) {
            o = (Orange) c;
            System.out.print("Orange: OK ");
        } else throw new ExcepcionCast("Orange: NO ");       
    }
 
    public static void castOrangeFruit(Fruit fr, Orange o) throws ExcepcionCast {
        if (o instanceof Fruit) {
            fr = (Fruit) o;
            System.out.print("Orange: OK ");
        } else throw new ExcepcionCast("Orange: NO ");       
    }
    
    public static void castOrangApple(Apple ap, Orange o) throws ExcepcionCast {

        if (!o.getClass().isInstance(ap)) {
            throw new ExcepcionCast("Apple: NO ");       
        }
    }

    public static void castOrangeSqueezable(Squeezable s, Orange o) throws ExcepcionCast {

        if (o instanceof Squeezable) {
            s = (Squeezable) o;
            System.out.print("Squeezable: OK ");
        } else throw new ExcepcionCast("Squeezable: NO ");             
    }

    public static void castOrangeCitrus(Citrus c, Orange o) throws ExcepcionCast {
        if (o instanceof Citrus) {
            c = o;
            System.out.print("Citrus: OK ");
        } else throw new ExcepcionCast("Citrus: NO ");       
    }

    public static void castOrangeOrange(Orange o, Orange o1) throws ExcepcionCast {
        if (o1 instanceof Orange) {
            o = o1;
            System.out.print("Orange: OK ");
        } else throw new ExcepcionCast("Orange: NO ");       
    }
    
    public static void checkObject(Fruit fr, Object obj, Apple ap, Citrus ct, Orange org) {
                
        //Prueba de Object
        System.out.print("Checkeo de cast de Object ---> ");
        
        try {
            castObjFruit(fr, obj);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castObjApple(ap, obj);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            Squeezable s = null;
            castObjSqueezable(s, obj);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castObjCitrus(ct, obj);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castObjOrange(org, obj);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
    }
    
    public static void checkFruit(Fruit fr, Object obj, Apple ap, Citrus ct, Orange org) {
        
        //Prueba de Fruit
        System.out.print("Checkeo de cast de Fruit ---> ");
        
        try {
            Fruit f=null;
            castFruitFruit(f, fr);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castFruitApple(ap, fr);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            Squeezable s = null;
            castFruitSqueezable(s, fr);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castFruitCitrus(ct, fr);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castFruitOrange(org, fr);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
    }
    
    public static void checkApple(Fruit fr, Object obj, Apple ap, Citrus ct, Orange org) {
        
        //Prueba de Fruit
        System.out.print("Checkeo de cast de Apple ---> ");
        
        try {
            castAppleFruit(ap, fr);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            Apple a = null;
            castAppleApple(a, ap);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            Squeezable s = null;
            castAppleSqueezable(s, ap);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castAppleCitrus(ct, ap);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castAppleOrange(org, ap);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }        
    }
    
    public static void checkCitrus(Fruit fr, Object obj, Apple ap, Citrus ct, Orange org) {
        
        //Prueba de Fruit
        System.out.print("Checkeo de cast de Citrus ---> ");
        
        try {
            castCitrusFruit(fr, ct);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castCitrusApple(ap, ct);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            Squeezable s = null;
            castCitrusSqueezable(s, ct);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            Citrus c = null;
            castCitrusCitrus(c, ct);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castCitrusOrange(org, ct);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
    }
    
    public static void checkOrange(Fruit fr, Object obj, Apple ap, Citrus ct, Orange org) {
        //Prueba de Orange
        System.out.print("Checkeo de cast de Orange ---> ");
        
        try {
            castOrangeFruit(fr, org);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castOrangApple(ap, org);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            Squeezable s = null;
            castOrangeSqueezable(s, org);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            castOrangeCitrus(ct, org);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }
        
        try {
            Orange o=null;
            castOrangeOrange(o, org);
        } catch (ExcepcionCast e) {
            System.out.print(e.getMessage());
        }        
    } 
}
